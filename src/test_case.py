from case import Where

def test_case1():
    check = Where()

    assert check.try_ex_1() == 0
    assert check.try_ex_2() == -3
    assert check.try_ex_3() == 3
    assert check.real_answer() == 138
