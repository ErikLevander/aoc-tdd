file = open("input.txt", "r")
data = file.read()


class Where():

    def try_ex_1(self):
        example_1 = "(())"
        test_example_1_up = example_1.count('(')
        test_example_1_down = example_1.count(')')
        return test_example_1_down - test_example_1_up

    def try_ex_2(self):
        example_2 = "(()(()("
        test_example_2_up = example_2.count('(')
        test_example_2_down = example_2.count(')')
        return test_example_2_down - test_example_2_up

    def try_ex_3(self):
        example_3 = ")())())"
        test_example_3_up = example_3.count('(')
        test_example_3_down = example_3.count(')')
        return test_example_3_down - test_example_3_up

    def real_answer(self):
        up = data.count('(')
        down = data.count(')')
        return up - down
